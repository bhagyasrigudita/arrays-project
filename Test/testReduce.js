let array = require('../arrays');
let reduce = require('../reduce');

function cb(startingValue, index) {
    return (startingValue * index);
}

let startingValue = array[0];

const outputArray = reduce(array, cb, startingValue);
console.log(outputArray);
