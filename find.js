function find(elements, cb) {

    if (elements == undefined || cb == undefined) {
        return [];
    }
    else {

        let outputArray = []

        for (let index = 0; index < elements.length; index++) {
            const True = cb(elements[index])
            if (True) {
                outputArray.push(elements[index])
            }
        }
        return outputArray;

    }

}


module.exports = find;
