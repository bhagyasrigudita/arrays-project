function map(elements, cb) {
    if (elements == undefined || cb == undefined) {
        return [];
    } else {
        let outputArray = [];
        for (let index = 0; index < elements.length; index++) {
            outputArray.push(cb(elements[index], index, elements));
        }
        return outputArray;
    }
    return [];
}

module.exports = map;
