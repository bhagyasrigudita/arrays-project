function each(elements, cb){

    if (elements == undefined || cb == undefined) {
        return [];
    }
    else{
        let outputArray = [];

        for(let index=0; index<elements.length; index++){

            outputArray.push(cb(elements[index]));
        }
        return outputArray;
    }

}

module.exports = each;
