let depth = []

function flatten(elements) {

    if (elements == undefined) {
        return []
    }
    else {

        for (let index = 0; index < elements.length; index++) {

            if (typeof (elements[index]) === 'number') {
                depth.push(elements[index])

            }
            
            else {

                flatten(elements[index])
            }

        }
        return depth
    }
}

module.exports = flatten;
