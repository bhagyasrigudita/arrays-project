function filter(elements, cb) {
    if (elements == undefined || cb == undefined) {
        return [];
    } else {
        let primeNumbers = [];
        for (let index = 0; index < elements.length; index++) {
            if (cb(elements[index], index, elements)) {
                primeNumbers.push(elements[index]);
            }
        }
        return primeNumbers;
    }
    return [];
}

module.exports = filter;
